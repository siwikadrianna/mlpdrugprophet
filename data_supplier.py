import pandas as pd


class DataSupplier:
    def __init__(self):
        self.data = None

    def load(self, file_path="drug200.csv"):
        self.data = pd.read_csv("drug200.csv")

    def unique_drugs_names(self):
        return self.data["Drug"].unique()

    def data_and_target(self):
        target = self.data[["Drug"]].values.tolist()

        target = [x[0] for x in target]

        target2 = [self.drug_mapper()[drug] for drug in target]
        data = self.data[["Age", "Sex", "BP", "Cholesterol", "Na_to_K"]]

        return data, target2

    def drug_mapper(self):
        target_length = len(self.unique_drugs_names())
        mapper = [[0 for i in range(target_length)]
                  for i in range(target_length)]

        # print(mapper)
        for i in range(target_length):
            mapper[i][i] = 1
            mapper[i]

        # print(mapper)
        return dict(zip(self.unique_drugs_names(), mapper))

    def vector_to_drug(self, vector):
        max_value = max(vector)
        max_index = list(vector).index(max_value)
        for k, v in self.drug_mapper().items():
            if v.index(1) == max_index:
                return k

    def sizes(self):
        print(type(self.data))
        print(len(self.data.columns)-1)
        return len(self.data.columns)-1, len(self.unique_drugs_names())
