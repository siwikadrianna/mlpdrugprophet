import pytest
import sys
import pathlib
from pandas import DataFrame

sys.path.append(str(pathlib.Path(__file__).parent.absolute()))

from data_normalisation_scaler \
    import DataNormalisationScaler  # nopep8


def test_can_create_data_normalsation_scaler_and_transform():
    data_normalisation_scaler = \
        DataNormalisationScaler(DataFrame([[0, 0], [1, 0.1], [2, 0.1]]))
    to_transform = DataFrame([[-1, 2], [-0.5, 6]])
    result = [list(subarr)
              for subarr in data_normalisation_scaler.transform(to_transform)]
    assert result == [[-0.5, 20], [-0.25, 60]]


def test_can_create_data_normalsation_scaler_with_textual_ordered_column():
    data_normalisation_scaler = \
        DataNormalisationScaler(DataFrame([['F', 'HIGH'],
                                           ['M', 'LOW'],
                                           ['F', 'NORMAL']
                                           ]))
    to_transform = DataFrame([['M', 'LOW'], ['F', 'HIGH']])
    result = [list(subarr)
              for subarr in data_normalisation_scaler.transform(to_transform)]
    assert result == [[1.0, 0.0],
                      [0.0, 1.0]]
