from neupy import algorithms, layers, plots
import sklearn.model_selection
from data_normalisation_scaler import DataNormalisationScaler
from data_supplier import DataSupplier
from pickle import dump


def get_network(*, train_size=0.85, epochs=95, inside_layers=[185, 185]):
    data_supplier = DataSupplier()
    data_supplier.load()
    data, target = data_supplier.data_and_target()
    data_normalization = DataNormalisationScaler(data)
    data = data_normalization.transform(data)

    # environment.reproducible()
    input_size, output_size = data_supplier.sizes()

    x_train, x_test, y_train, y_test = \
        sklearn.model_selection.train_test_split(
            data, target, train_size=train_size
        )

    cgnet = algorithms.ConjugateGradient(
        network=[
            layers.Input(input_size),
            *[layers.Sigmoid(layer) for layer in inside_layers],
            layers.Sigmoid(output_size),
        ],
        show_epoch=5,
        verbose=True,
    )

    cgnet.train(x_train, y_train, x_test, y_test, epochs=epochs)
    cgnet.plot_errors()
    return cgnet, data_normalization, data_supplier


if __name__ == '__main__':
    network, scaler, datas = get_network()
    dump(network, open("net.pickle", "wb"))
    dump(scaler, open("scaler.pickle", "wb"))
    dump(datas, open("datas.pickle", "wb"))
