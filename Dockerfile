FROM tensorflow/tensorflow:latest
EXPOSE 80 
COPY . /app
RUN pip install --trusted-host pypi.python.org --trusted-host files.pythonhosted.org --trusted-host pypi.org -r /app/req.txt
RUN cd /app
CMD cd /app/&& python server.py
