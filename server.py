from flask import Flask
from get_network import get_network
from pickle import load
from pandas import DataFrame

app = Flask(__name__)

network = load(open("net.pickle", "rb"))
scaler = load(open("scaler.pickle", "rb"))
datas = load(open("datas.pickle", "rb"))


@app.route('/<int:p1>/<string:p2>/<string:p3>/<string:p4>/<float:p5>')
def show_med_data(p1, p2, p3, p4, p5):
    scalert = scaler.transform(DataFrame([[p1, p2, p3, p4, p5]]))

    return datas.vector_to_drug(network.predict(scalert)[0])


app.run("0.0.0.0", port=80)
